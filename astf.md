---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Aide à la composition  d'oeuvres pour tambours et percussions
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 2
mandants:
  - Association Suisse des Tambours et Fifres (ASTF) 
professeurs co-superviseurs:
  - ???
  - ???
mots-clé: [Musique, Ecriture, Reconnaissance de formes]
langue: [F, E]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth,height=\textheight]{img/compo.png}
\end{center}
```

## Description/Contexte

Le comité central de l'Association Suisse des Tambours et Fifres (CC/ASTF - www.astf.ch) souhaite mettre à dispostion des compositeurs d'oeuvres pour tambours et percussions un programme leur facillitant l'écriture et la première écoute des pièces qu'ils créent.

L'idée du CC/ASTF est de développer une application sur tablette permettant aux compositeurs d'éditer leurs compositions à l'aide d'un crayon et que le programme soit capable de reconnaître les notes, la rythmique et la dynamique afin de les retranscrire dans un format intermédiare. Ce format devrait permettre l'édition de la composition et de sa correction, la génération d'un document PDF selon les standards actuels de l'ASTF ainsi de jouer la pièce sur la tablette ou sur un ordinateur.

En finalité, le programme doit permettre d'écrire des pièces pour tambours seuls, des pièces à plusieurs voies, des pièces pour les percussions ainsi que des pièces de show avec des figures "acrobatiques".

Le programme doit être multiplateforme.

## Objectifs/Tâches

