---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2021/2022
titre: Cible pour surveillance d'installations gazières
filières:
  - ISC
nombre d'étudiants: 2
mandants:
  - ISC
professeurs co-superviseurs:
  - Serge Ayer
mots-clé: [microprocesseur, programmation, communication, traitement signal audio]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.35\textwidth,height=\textheight]{img/gaznat.jpg}
\end{center}
```
## Description/Contexte

L'entreprise Gaznat est un acteur de confiance sur la scène énergétique. L'entreprise s’engage à exploiter son réseau de gaz naturel et/ou renouvelable et ses infrastructures de manière sûre, efficace et durable. 

Pour la surveillance de leurs installations gazière, Gaznat souhaiterait développer un système qui aurait les caractéristiques suivantes :
 
- Surveillance acoustique du local gaz (poste de détente et comptage).
- Détection d’un changement anormal du niveau sonore afin de déclencher une alarme.
- Enregistrement des sons durant les périodes anormales afin d’analyser l’événement.
- Communication de quelques paramètres (par ex. niveaux sonores) par interface Modbus TCP au système SCADA.
- Utilisation d'un microphone certifié pour zone ATEX (p.ex. http://www.pcb.com/sensors-for-test-measurement/acoustics/specialty-microphones/hazardous-approved-microphone-preamplifier-ex378b02)
- Interface (préamplificateur) pour acquisition du signal sonore à développer.
- Traitement de signal pour calcul du niveau sonore (idéalement valeur en dBa).
 
## Objectifs 

L'objectif principal est le développement du système sur une base de microcontrôleur à définir.

Ce système devra:

- Intégrer un microphone certifié pour zone ATEX.
- Faire l'acquisition du signal afin de le traiter.
- Réaliser le traitement de signal, selon les possibilités du plus simple jusqu’au plus sophistiqué(par ex. FFT, traitements différenciés par fréquence, etc…).
- Permettre un paramétrage simple et un accès aisé aux fichiers sons.

## Tâches

Les tâches principales sont les suivantes:

- Elaborer un cahier des charges en collaboration avec les responsables du projet
- Etudier l'état de l'art
- Concevoir et réaliser une maquette fonctionnelle
- Tester et valider la maquette
- Rédiger une documentation adéquate
